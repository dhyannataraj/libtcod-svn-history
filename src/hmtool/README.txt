libtcod 1.4.0 height map tool

Actually this is currently more a curiosity than a real useful tool.
It allows you to tweak a heightmap by hand and generate the corresponding C++ code.
Sorry, no C version yet, but the generated C++ code should be easily converted by hand to C.

