#ifndef _GUI_HPP
#define _GUI_HPP

#ifdef VISUAL_STUDIO
#pragma warning(disable:4996)
#endif

#include "widget.hpp"
#include "button.hpp"
#include "radiobutton.hpp"
#include "container.hpp"
#include "label.hpp"
#include "statusbar.hpp"
#include "textbox.hpp"
#include "slider.hpp"
#include "togglebutton.hpp"
#include "toolbar.hpp"
#include "vbox.hpp"
#include "hbox.hpp"
#include "image.hpp"

#endif

